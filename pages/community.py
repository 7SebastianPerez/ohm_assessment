from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions.connect_to import connectTo, getConnection

import mysql.connector

from functions import app
from models import User

# engine = create_engine('mysql://root:root@localhost/ohm_assesment')

def get_community_info(cursor):

    user_info = {}
    id_index = 0
    display_index = 3
    tier_index = 7
    point_balance_index = 11
    try:
        cursor.execute("select * from user")
        for user_data in cursor:
            user_id = user_data[id_index]
            user_info[user_id] = {}
            user_info[user_id]["display_name"] = user_data[display_index]
            user_info[user_id]["tier"] = user_data[tier_index]
            user_info[user_id]["point_balance"] = user_data[point_balance_index]
            user_info[user_id]["phones"] = ""
    except:
        print("Failed getting user community info")

    id_index = 1
    rel_lookup_index = 2
    phone_index = 3
    try:
        cursor.execute("select * from rel_user_multi;")
        for phone_data in cursor:
            if phone_data[rel_lookup_index] != "PHONE":
                continue
            curr_id = phone_data[id_index]
            if curr_id not in user_info:
                continue
            user_info[curr_id]["phones"] += (phone_data[phone_index] + "  ") # definitely a better way
    except:
        print("Failed getting community info")

    return user_info

@app.route('/community', methods=['GET'])
def community():

    connection = connectTo('access')
    cursor = connection.cursor()
    community_info = get_community_info(cursor)
    # ran out of time but i would change this by using a join and order by date to get the data
    print(community_info)
    cursor.close()
    connection.close()

    rows = []
    data = community_info.values()
    ids = community_info.keys()
    for i in range(len(data)):
        color = "default"
        if i % 2 == 0:
            color = "success"
        rows.append({"id":ids[i], "color": color, "data":data[i]})

    return render_template("community.html", rows = rows )
