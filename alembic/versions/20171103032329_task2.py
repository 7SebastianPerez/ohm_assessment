"""task2

Revision ID: 2c0f7bcebbd0
Revises: 00000000
Create Date: 2017-11-03 03:23:29.901263

"""

# revision identifiers, used by Alembic.
revision = '2c0f7bcebbd0'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("update user set point_balance=1000 where user_id=2;")
    op.execute("update user set tier='Bronze' where user_id=3;")


def downgrade():
    pass
